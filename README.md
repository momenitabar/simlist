![N|Solid](http://www.upsara.com/images/q360663_.png)

# SimList
It's SimList package that easily converts every kinds of inputting data to a list.In addition it could generate a list with different ability in form of : sorting,custom columns, setting custom title, setting custom configs over the columns.
The SimList could help you to make simple list with awesome features like piece of cake.

## Features
 - Easy to use
 - Customize columns sorting
 - Customize columns visibility
 - Customize configuration
 - Frontend friendly

## HOWTO
The SimList uses Dependency injection to inject your data.it would let you to being able working with every kind of data, just need to define a resource that was implemented by abstraction layer of this package,By the same token I want to explain the SimpList by way of example, 

#### DatabaseResource
```php
class DatabaseResource implements SimListResourceInterface
{
    /**
     * @var Model
     */
    private $model;

//Set model that we need it's data
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

//Get data from database
    public function getData(): array
    {
        return $this->model::all()->toArray();
    }
}
```

#### FooController
```php
<?php

// Grap all the users from database by creating SimList instance that need to pass users as parameter.
        $simList = new SimList(new DatabaseResource(new User()));
// Then we are starting to define columns
        $results = $simList
            ->setColumn('name', 'Name', ['length' => 100, 'percentage' => 10, 'type' => 'string' , 'sortable' => true])
            ->generate();
// Finally pass the results to the view
        return view('simlist', $results);
```

#### Test
```php
php artisan test
```

## HOW TO USE (VIDEO)

- [Database Resource](https://www.youtube.com/watch?v=2MKHzi71q8M)
- [SimList Controller](https://www.youtube.com/watch?v=1v7z5vzWfpQ)
- [SimList View](https://www.youtube.com/watch?v=hRiW5n5IKlM)
- [Result](https://www.youtube.com/watch?v=7AOjHRfDeds)
