<?php

namespace Tests\Unit\SimList\Resources;

use App\Contracts\SimListResourceInterface;

class ArrayResource implements SimListResourceInterface
{

    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
