<?php

namespace Tests\Unit\SimList;

use Tests\TestCase;
use Tests\Unit\SimList\Resources\ArrayResource;

class SimListResourceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_array_resource_count()
    {
        $data = [
            ['name' => 'Jack','age' => 18],
            ['name' => 'Jone','age' => 19],
        ];

        $resource = new ArrayResource($data);

        $result = $resource->getData();

        $this->assertCount(2,$result);
    }

    public function test_array_resource_check_value()
    {
        $data = [
            ['name' => 'Jack','age' => 18],
            ['name' => 'Jone','age' => 19],
        ];

        $resource = new ArrayResource($data);

        $result = $resource->getData();

        $this->assertEquals($result[0]['name'],$data[0]['name']);
    }
}
