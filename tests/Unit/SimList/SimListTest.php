<?php

namespace Tests\Unit\SimList;

use App\SimList\SimList;
use PHPUnit\Framework\TestCase;
use Tests\Unit\SimList\Resources\ArrayResource;

class SimListTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_simlist_set_column_check_title()
    {
        $data = [
            ['name' => 'Jack','age' => 18],
            ['name' => 'Jone','age' => 19],
        ];

        $resource = new ArrayResource($data);

        $list = new SimList($resource);

        $result = $list->setColumn('name','Name')
            ->generate();

        $this->assertEquals($result['columns']['name']['title'],'Name');

    }

    public function test_simlist_check_count_of_rows()
    {
        $data = [
            ['name' => 'Jack','age' => 18],
            ['name' => 'Jone','age' => 19],
        ];

        $resource = new ArrayResource($data);

        $list = new SimList($resource);

        $result = $list->setColumn('name','Name')
            ->generate();

        $this->assertCount(1,$result['rows'][0]);
    }

    public function test_simlist_final_count_rows()
    {
        $data = [
            ['name' => 'Jack','age' => 18],
            ['name' => 'Jone','age' => 19],
        ];

        $resource = new ArrayResource($data);

        $list = new SimList($resource);

        $result = $list->setColumn('name','Name')
            ->generate();

        $this->assertCount(count($data),$result['rows']);
    }
}
