<?php


namespace App\Resources;


use Illuminate\Database\Eloquent\Model;

class DatabaseResourceAdapter implements \App\Contracts\SimListResourceInterface
{

    /**
     * @var Model
     */
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getData()
    {
        return $this->model::all();
    }
}
