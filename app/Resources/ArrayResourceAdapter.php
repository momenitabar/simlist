<?php


namespace App\Resources;


class ArrayResourceAdapter implements \App\Contracts\SimListResourceInterface
{

    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}
