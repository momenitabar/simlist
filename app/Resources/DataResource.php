<?php


namespace App\Resources;


use App\Contracts\SimListResourceInterface;

class DataResource implements \App\Contracts\SimListResourceInterface
{

    /**
     * @var SimListResourceInterface
     */
    private $resource;

    public function __construct(SimListResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    public function getData()
    {
        $data = $this->resource->getData();
        if (! is_array($data)){
            return $data->toArray();
        }

        return $data;
    }
}
