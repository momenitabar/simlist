<?php


namespace App\SimList;


use App\Contracts\SimListResourceInterface;
use App\Contracts\SortStrategy\SortStrategy;
use Illuminate\Pagination\LengthAwarePaginator;

class SimList
{

    /**
     * @var SimListResourceInterface
     */
    private $resource;
    private $column = [];
    protected $row = [];

    public function __construct(SimListResourceInterface $resource)
    {
        $this->resource = $resource;
        $this->row = $this->resource->getData();
    }
//Setting columns by custom data
    public function setColumn(string $column, string $title, array $config = [])
    {
        foreach ($this->row as $key => $value) {
            if (isset($value[$column])) {
                $this->column['columns'][$column] = ['title' => $title, 'config' => $config];
            }
        }

        return $this;
    }
//Generating result
    public function generate()
    {
        $result = [];
        foreach ($this->row as $key => $row) {
            foreach (array_keys($this->column['columns']) as $column) {
                if (isset($row[$column])) {
                    $result['rows'][$key][$column] = $row[$column];
                }
            }
        }
        $result['columns'] = $this->column['columns'];

        return $result;
    }

    public function setSort(string $key,string $sortType = 'ASC')
    {
        try {
            if (!isset($this->row[0][$key])){
                throw new \Exception('Empty');
            }
            $strategyType = gettype($this->row[0][$key]);
            $strategy = new SortStrategy($strategyType,$this->row);
            $this->row = $strategy->sort($key,$sortType);
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

        return $this;
    }

}
