<?php


namespace App\Contracts;


interface SimListResourceInterface
{
    public function getData();
}
