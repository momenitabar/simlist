<?php


namespace App\Contracts\SortStrategy\Types;


abstract class AbstractTypeStrategy
{

    /**
     * @var array
     */
    protected $rows;

    abstract public function sort(string $key,string $sortType);

    /**
     * @param array $rows
     */
    public function setRows(array $rows): void
    {
        $this->rows = $rows;
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

}
