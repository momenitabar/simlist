<?php
namespace App\Contracts\SortStrategy\Types;

class IntegerStrategy extends AbstractTypeStrategy
{

    public function sort(string $key,string $sortType)
    {
        usort($this->rows,function ($x , $y) use ($key,$sortType){
            if ($sortType === 'DESC'){
                return $y[$key] - $x[$key];
            }
            return $x[$key] - $y[$key];
        });
    }

}
