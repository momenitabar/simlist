<?php
namespace App\Contracts\SortStrategy\Types;

class StringStrategy extends AbstractTypeStrategy
{

    public function sort(string $key,string $sortType)
    {
        usort($this->rows,function ($x , $y) use ($key,$sortType){
            if ($sortType === 'DESC'){
                return strcasecmp($y[$key] , $x[$key]);
            }
            return strcasecmp($x[$key] , $y[$key]);
        });
    }
}
