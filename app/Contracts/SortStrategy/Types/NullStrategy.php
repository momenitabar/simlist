<?php
namespace App\Contracts\SortStrategy\Types;

use App\Contracts\SortStrategy\SortStrategy;

class NullStrategy extends AbstractTypeStrategy
{

    public function sort(string $key)
    {
        return $this->rows;
    }
}
