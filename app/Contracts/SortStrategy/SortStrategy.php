<?php


namespace App\Contracts\SortStrategy;


use App\Contracts\SortStrategy\Types\AbstractTypeStrategy;
use App\Contracts\SortStrategy\Types\DateStrategy;
use App\Contracts\SortStrategy\Types\IntegerStrategy;
use App\Contracts\SortStrategy\Types\NullStrategy;
use App\Contracts\SortStrategy\Types\StringStrategy;

class SortStrategy
{

    protected $type;

    /**
     * @var AbstractTypeStrategy
     */
    protected $strategy;

    protected $rows;

    public function __construct(string $type,array $rows)
    {
        $this->type = $type;
        $this->rows = $rows;
    }

    public function sort(string $key,string $sortType)
    {
        switch ($this->type){
            case 'string' :
                $this->strategy = new StringStrategy();
                break;
            case 'integer' :
                $this->strategy = new IntegerStrategy();
                break;
            default :
                $this->strategy = new NullStrategy();
                break;
        }
        $this->strategy->setRows($this->rows);
        $this->strategy->sort($key,$sortType);

        return $this->strategy->getRows();
    }

}
