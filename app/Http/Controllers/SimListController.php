<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Resources\DatabaseResourceAdapter;
use App\Resources\DataResource;
use App\SimList\SimList;

class SimListController extends Controller
{

    public function result()
    {
        $model = new User();
        $adapter = new DatabaseResourceAdapter($model);
        $resource = new DataResource($adapter);
        $simlist = new SimList($resource);

        $results = $simlist
            ->setColumn('name','Name',['percentage' => 90])
            ->setColumn('age','Age',['percentage' => 10])
            ->setSort('age','ASC')
            ->setPagination(3)
            ->generate();
        return view('simlist' , $results);
    }
}
