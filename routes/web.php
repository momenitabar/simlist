<?php

use Illuminate\Support\Facades\Route;

Route::get('/',[
    'uses' => '\App\Http\Controllers\SimListController@result',
    'as' => 'result',
]);
