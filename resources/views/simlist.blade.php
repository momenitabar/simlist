<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>
<center>
    <br>
    <br>
    <table class="table table-bordered table-hover col-md-5 mt-5">
        <thead id="thead" class="thead-light">
        <tr>
            @foreach($columns as $keys => $values)
                <th width="{{$values['config']['percentage']}}%">
                    {{$values['title']}}
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody id="tbody">
        @foreach($rows as $row)
            <tr>
                @foreach($row as $item)
                    <td>
                        {{$item}}
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</center>
</body>
</html>
<script>
    $(document).ready(function(){
        $("#searchInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
